FREENOM 1  "JUL 2022" "User Manuals"
====================================

NAME
----

freenom - Script to Freenom.com Domain Renewal and update Dynamic DNS


SYNOPSIS
--------

`freenom` [`OPTIONS`] *argument*


DESCRIPTION
-----------

`freenom` this shell script makes sure your Freenom domains don't expire by
auto renewing them.
It's original functionality of updating an A record with the clients ip address
is also retained.

You'll need to have already registered an account at Freenom.com with at least
one (free) domain added, before you can run the script.


USAGE
-----

`freenom` [`-l`] [`-d`]

`freenom` [`-r` *domain* OR `-a`] [`-s` *subdomain*]

`freenom` [`-u` *domain*] [`-s` *subdomain*] [`-m` *ip*] [`-f`]

`freenom` [`-z` *domain*]


OPTIONS
-------

`-l`
  List all domains and id's in account
  add [-d] to show renewal Details

`-r`
  Renew *domain* or use '-r -a' to update All
  add [-s] to update *Subdomain*

`-u`
  Update *domain* A record with current ip
  add [-s] to update *Subdomain* record
  add [-m *ip*] to Manually update static *ip*
  add [-f] to Force update on unchanged ip

`-z`
  Zone for *domain*, shows dns records

`-4`
  Use ipv4 and modify A record on [-u] (default)

`-6`
  Use ipv6 and modify AAAA record on [-u]

`-c`
  Config *file* to be used, instead freenom.conf

`-i`
  Ip commands list used to get current ip

`-o`
  Output renewals, shows html file(s)

EXAMPLES:
---------

`freenom-r example.com`

`freenom -c /etc/mycustom.conf -r -a`

`freenom -u example.com -s mail`

NOTES:
------

When [`-u`] or [`-r`] is used with argument *domain* any settings in script or
config file are overridden


CONFIGURATION
-------------

Settings can be changed in the script itself or set in a separate config file
(default). Every setting has comments with possible options and examples.

First edit config (/etc/freenom.conf) and *set your email and password* which
you use to sign-in to freenom.com

- The default filename is "freenom.conf" in the location */etc/freenom.conf*

- You can also use `freenom -c /path/to/file.conf`

- To optionally put config in the script itself instead: copy settings from
conf into freenom (before "Main")


TESTING
-------

Test the script by running `freenom -l` and make sure your domains are listed.
To update A record or Renew domains, see `freenom -h`


Scheduling
----------

Optionally you can schedule the script to run automatically. The installer
creates */etc/cron.d/freenom* or systemd timers in 'system mode' so the script
runs at certain intervals. It will output a message with instructions on how to
set your domain(s) to renew/update or renew all:

`- Cron:`

edit the created file */etc/cron.d/freenom*, uncomment line(s)

`- Systemd:`

`systemctl enable --now freenom-renew-all.timer`

`systemctl enable --now freenom-update@example.tk.timer`

`systemctl enable --now freenom-update@mysubdom.example.tk.timer`

If systemd is not available on your system the installer will use cron instead.


Manually setup cron
-------------------

`Steps:`

1) Copy cron.d/freenom from repo to */etc/cron.d/freenom*

2) Edit file to specify domain(s)

Example

0 9 * * 0 root bash -c 'sleep $((RANDOM \% 60))m; /usr/local/bin/freenom -r -a'

0 * * * * root bash -c 'sleep $((RANDOM \% 15))m; /usr/local/bin/freenom -u
example.tk'

0 * * * * root bash -c 'sleep $((RANDOM \% 15))m; /usr/local/bin/freenom -u
example.tk -s mysubdom'

This first line in this example will run the script with "renew all domains"
options every week on Sunday between 9.00 and 10.00

The second line updates the A record of example.tk with the current client ip
address, at hourly intervals

Manually setup systemd
----------------------

Add one or more "timer(s)
(*https://www.freedesktop.org/software/systemd/man/systemd.timer.html*)"

Thanks to sdcloudt(*https://github.com/sdcloudt*) you can use the template
units from the systemd dir.

`1)` Copy files from repo to: */lib/systemd/system*

`2)` Create a service instance for your domain(s) by creating symlinks (or use
   `systemctl enable`(*https://github.com/mkorthof/freenom-script#Scheduling*))

`3)` Reload systemd

Example

`- create symlinks:`

    mkdir /path/to/systemd/timers.target.wants

    ln -s /path/to/systemd/freenom-renew-all.service
    /etc/systemd/user/timers.target.wants/freenom-renew-all.service

`- optional to renew a specific domain, replace freenom-renew-all by:`

    ln -s /path/to/systemd/freenom-renew@.service
    /etc/systemd/user/timers.target.wants/freenom-renew@example.tk.service

`- then reload systemd:`

    systemctl daemon-reload

In case of any errors make sure you're using the correct paths and
"freenom.conf" is setup. Check `systemctl status` "unit" and logs.

`Note: to use 'user mode' instead of system mode replace "/system" by "/user"
and use 'systemctl --user'`


NOTIFICATIONS
-------------

Email
-----

To enable email alerts, make sure MTA is set; the default is
*/usr/sbin/sendmail*. Emails will be sent on renewal or update errors. If you
do not have/want an MTA installed you could use bashmail(*https://git.io/JJdto*)
instead.

If you want to receive the alerts on a different email address than
freenom_email set RCPTTO. You can also set an optional "From" address:
MAILFROM="Freenom Script *freenom-script@example.tk*"

Leaving MTA empty or commented disables alerts.


Apprise
-------

Uses external lib to send notification to many services like Telegram, Discord,
Slack, Amazon SNS, MS Teams etc.

To enable Apprise(*https://github.com/caronc/apprise*) notifications, make sure
APPRISE is set to the location where you installed the Apprise CLI; the default
is /usr/bin/apprise. You must also set the `APPRISE_SERVER_URLS` array to
contain one or more server URLs. Notifications are sent to all of the listed
server URLs. As with email notifications, Apprise notifications are sent on
renewal or update errors.

For details on how to construct server URLs, refer to supported notifications(*https://github.com/caronc/apprise#supported-notifications*).

Leaving the `APPRISE_SERVER_URLS` array empty disables Apprise notifications.


Optional Overrides
------------------

Default settings such as retries and timeouts can be changed in config, they
are however OK to leave as-is.

See */usr/share/doc/freenom/Overrides.md*


DynDNS
------

To update A or AAAA records the nameservers for the domain must be set to the
default Freenom Name Servers.

- As value your current ip address will be used ("Target").

- An record will be added if there is none or modified if the record already
exists.


IP Address
----------

To get your current ip address from a number of public services the script uses
3 methods:

`- HTTP method`: curl https://checkip.amazonaws.com

`- DNS method`: dig TXT +short o-o.myaddr.l.google.com @ns1.google.com

`- Manually`: you can set a static ip address instead of auto detect

There are a few more services defined for redundancy, the script will choose
one at random. By default it will retry 3 times to get ip.

Once your ip is found it's written to "freenom_domain.ml.ip4" (or 'ip6').
Same if freenom returns dnserror "There were no changes". This is to prevent
unnecessary updates in case the ip is unchanged.

To force an update you can remove this file which is located in the default
output path: "/var/log".

To manually update: set `freenom_static_ip="your ip"` and
`freenom_update_manual="1"`, or use the [-m] option.


ISSUES
------

Make sure 'curl' and/or 'dig' is installed (e.g. debian: bind9-dnsutils or
redhat: bind-utils)

In case of issues try running curl and dig command manually.

`- To list all 'get ip' commands run `freenom -i` (or grep getIp freenom.conf)`

`- To disable IPv6:` `set freenom_update_ipv="4"`

`- To disable dig:` `set freenom_update_dig="0"`



SOURCES
-------

`- Original script:` *https://gist.github.com/a-c-t-i-n-i-u-m/bc4b1ff265b277dbf195* 

`- Updated script:`
*https://gist.github.com/pgaulon/3a844a626458f56903d88c5bb1463cc6*

`- Reference:` *https://github.com/dabendan2/freenom-dns* (nodejs/npm)

`- Reference:` *https://github.com/patrikx3/freenom* (nodejs/npm)


CHANGES
-------

See */usr/share/doc/freenom/CHANGES.md*


SEE ALSO
--------

cron(1), crontab(1), crontab(5), systemctl(1), [Freenom.com](
https://my.freenom.com/knowledgebase.php)


AUTHOR
------

`Marius Korthof <m@mari.us.com>`
    Based on github.com/mkorthof/freenom-script/README.md

`Braulio Henrique Marques Souto <braulio@disroot.org>`
    Created this man-page on markdown, and used go-md2man to generate
    the manual file
